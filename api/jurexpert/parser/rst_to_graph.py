import json
import itertools
import os
import pathlib
import ujson
from peewee import fn

from lark import Lark, Tree, Transformer, v_args
from lark.reconstruct import Reconstructor
from kt import TokyoTyrant, TT_TABLE, QueryBuilder, constants

import dateparser
from datetime import datetime

from api.jurexpert.model.graph import Hexastore
from api import log

from api.jurexpert.model.jurexpert import Jurexpert

db = Jurexpert()
db.clear()
db.set_index('_collection', constants.INDEX_STR)
db.set_index('_key', constants.INDEX_STR)
db.set_index('_date', constants.INDEX_STR)
db.set_index('description', constants.INDEX_STR)
db.set_index('title', constants.INDEX_STR)
db.set_index('name', constants.INDEX_STR)

graph = Hexastore(database=db)

tmp_values = {
    "rst_filename": "",
    "decision_title": "",
    "decision_key": "",
    "case_key": "",
    "decision_date": "",
    "topic": "",
    "annotation": ""
}

def rst_to_graph():
    global tmp_values
    db.clear()
    with open(db.grammar) as grammar_file:
        entity_grammar = grammar_file.read()

    parser = Lark(
        entity_grammar,
        start="start",
        lexer="standard"  # or dynamic, see more: https://lark-parser.readthedocs.io/en/latest/parsers/
    )

    path = pathlib.Path(db.rst_path)
    files = list(path.glob('*.rst'))
    for f in files:
        if f.is_file():
            db.doc = {}
            current_file = open(f, "r")
            tmp_values["rst_filename"] = current_file
            rst_text = current_file.read()

            parse_tree = parser.parse(rst_text)
            rst_text_cleared_from_terminals = "".join(EvalExpressions().transform(parse_tree))

            # { Add "content" vertex
            found = db.contains("content", "head_key", tmp_values["decision_key"])
            db_key = found["_key"] if found else db.new_key("content")

            grammar_key = "content"
            doc_key = db_key + "::" + grammar_key
            db.doc[doc_key] = found if found else {
                "_key": db_key,
                "_collection": "content",
                # "body": tuple(("text", rst_text_cleared_from_terminals)),
                "body": rst_text_cleared_from_terminals,
                "head_key": tmp_values["decision_key"]
            }
            if not found:
                db[db_key] = db.doc[doc_key]
            db.doc[doc_key]["_doc_key"] = "content"
            # }

            # Add edges
            # for item in db.doc:
            # print(db.doc[item]["_doc_key"])
            # Iterate through db.doc dictionary
            # get "_doc_key" for each key
            # run edge = spo(s=cur_item, o=next_item)
            # db["edge.spo::cur::edge::next"] = { s: cur, p: edge, o: next }
            # db["edge.pos::edge::next::cur"] = { s: cur, p: edge, o: next }
            graph = Hexastore(db)
            for cur in db.doc:
                for key in db.doc:
                    if key != cur:
                        v1 = db.doc[cur]
                        v2 = db.doc[key]
                        # print("-- v2 -- ", key, v2)
                        edge = db.allowed_by_schema(s=v1["_doc_key"], o=v2["_doc_key"], schema=db.schema_doc)
                        if edge:
                            # print("== V1->V2 EDGE == ", v1["_key"], edge, v2["_key"])
                            graph.store(s=v1["_key"], p=edge[0], o=v2["_key"])

            current_file.close()

            tmp_values = {
                "decision_title": "",
                "decision_key": "",
                "case_key": "",
                "decision_date": "",
                "topic": "",
                "annotation": ""
            }

    # print(parser.parse(text).pretty())
    # print(parse_tree)


    print("\n List of decisions")
    edges = graph.query(p='is_part_of')
    for ed in edges:
        result = edges[ed]
        print(db[result['o']])


    return "rst_text_cleared_from_terminals"

@v_args(inline=True)
class EvalExpressions(Transformer):
    def start(self, *all_together):
        # if case number was not labeled in rst document by human
        # then skip this document (do not add it to DB)
        if tmp_values["case_key"] == "":
            log.warn("Case does not marked in the RST document '{}'".format(tmp_values["rst_filename"]))
            return all_together

        # { Add "decision" vertex
        found = db.contains("decision", ("title", "case_key"), (tmp_values["decision_title"], tmp_values["case_key"]))
        db_key = found["_key"] if found else db.new_key("decision")
        tmp_values["decision_key"] = db_key

        grammar_key = "decision"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "decision",
            "_date": tmp_values["decision_date"],
            "title": tmp_values["decision_title"],
            "case_key": tmp_values["case_key"],
            "annotation": tmp_values["annotation"],
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "decision"
        # }

        # { Add "topic" vertex
        found = db.contains("topic", "title", tmp_values["topic"])
        db_key = found["_key"] if found else db.new_key("topic")

        grammar_key = "topic"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "topic",
            "title": tmp_values["topic"]
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "topic"
        # }

        '''
        for item in db:
            print(item, " = ", db[item])
            print("----------------------------------------------------")
        '''

        
        print("\n____MAKE SOME SEARCH____")
        print("Who was represented by attorney Smirnova E.V.?")

        counsel = db.contains("counsel", "name", "Смирнова Е.В.")
        counsel_key = counsel["_key"]
        print(counsel)

        edges = graph.query(p='represented_by', o=counsel_key)
        for ed in edges:
            result = edges[ed]
            print(db[result['s']])


        print("\n____MAKE TRVERSING____")
        print("In what Court judges made decision in 2016:")

        X = graph.v.X  # Create a variable reference.
        Y = graph.v.Y  # Create a variable reference.
        Z = graph.v.Z  # Create a variable reference.
        results = graph.search(
            (X, 'dated', "date.2016"),  # It's possible to set only year "date.2017"
            (Y, 'makes', X),
            (Z, 'assigns', Y))
        # print(results['X'])
        # print(results['Y'])
        # print("Traverse result X: ", results['X'])
        # print("Traverse result Y: ", results['Y'])
        # print("Traverse result Z: ", results['Z'])
        for rz in results['Z']:
            print(db[rz])

        print("___END OF SEARCH__")
        
        return all_together


    def court(self, title):
        found = db.contains("court", "title", title)
        db_key = found["_key"] if found else db.new_key("court")

        grammar_key = "court"
        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "court",
            "title": str(title)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "court"
        return title

    def case(self, number):
        found = db.contains("case", "number", number)
        db_key = found["_key"] if found else db.new_key("case")

        grammar_key = "case"
        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "case",
            "number": str(number)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "case"
        tmp_values["case_key"] = db_key
        return number

    def judge(self, name):
        found = db.contains("judge", "name", name)
        db_key = found["_key"] if found else db.new_key("judge")

        grammar_key = "judge"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "judge",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "judge"
        return name

    def court_city(self, title):
        found = db.contains("court_city", "title", title)
        db_key = found["_key"] if found else db.new_key("court_city")

        grammar_key = "court_city"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "court_city",
            "title": str(title)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "court_city"
        return title

    def court_area(self, area):
        found = db.contains("court_area", "area", area)
        db_key = found["_key"] if found else db.new_key("court_area")

        grammar_key = "court_area"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "court_area",
            "area": str(area)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "court_area"
        return area

    def claimant(self, name):
        found = db.contains("party", "name", name)
        db_key = found["_key"] if found else db.new_key("party")

        grammar_key = "claimant"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "party",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "claimant"

        return name

    def defendant(self, name):
        found = db.contains("party", "name", name)
        db_key = found["_key"] if found else db.new_key("party")

        grammar_key = "defendant"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "party",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "defendant"
        return name

    def third_party(self, name):
        found = db.contains("party", "name", name)
        db_key = found["_key"] if found else db.new_key("party")

        grammar_key = "third_party"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "party",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "third_party"
        return name

    def counsel_of_claimant(self, name):
        found = db.contains("counsel", "name", name)
        db_key = found["_key"] if found else db.new_key("counsel")

        grammar_key = "counsel_of_claimant"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "counsel",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "counsel_of_claimant"
        return name

    def counsel_of_defendant(self, name):
        found = db.contains("counsel", "name", name)
        db_key = found["_key"] if found else db.new_key("counsel")

        grammar_key = "counsel_of_defendant"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "counsel",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "counsel_of_defendant"
        return name

    def counsel_of_third_party(self, name):
        found = db.contains("counsel", "name", name)
        db_key = found["_key"] if found else db.new_key("counsel")

        grammar_key = "counsel_of_third_party"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "counsel",
            "name": str(name)
        }
        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "counsel_of_third_party"
        return name

    def date_of_decision(self, date_of_decision):
        try:
            dod = dateparser.parse(str(date_of_decision), languages=['ru'])
        except Exception:
            log.error("Could not convert data_of_decision string '{}' to date object".format(date_of_decision))

        tmp_values["decision_date"] = "{0}{1}{2}".format(str(dod.year), str(dod.month).zfill(2), str(dod.day).zfill(2))
        db_key = db.date_key(dod.year, dod.month, dod.day)
        found = db.get(db_key)
        grammar_key = "date_of_decision"
        doc_key = db_key + "::" + grammar_key

        db.doc[doc_key] = found if found else {
            "_key": db_key,
            "_collection": "date",
            "year": dod.year,
            "month": dod.month,
            "day": dod.day
        }

        if not found:
            db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "date_of_decision"
        return date_of_decision

    def money_base_required_by_claimant(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_base_required_by_claimant"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_base_required_by_claimant",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_base_required_by_claimant"
        return amount

    def money_base_set_by_court(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_base_set_by_court"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_base_set_by_court",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_base_set_by_court"
        return amount

    def money_moral_compensation_required_by_claimant(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_moral_compensation_required_by_claimant"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_moral_compensation_required_by_claimant",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_moral_compensation_required_by_claimant"
        return amount

    def money_moral_compensation_set_by_court(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_moral_compensation_set_by_court"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_moral_compensation_set_by_court",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_moral_compensation_set_by_court"
        return amount

    def money_penalty_required_by_claimant(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_moral_compensation_required_by_claimant"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_moral_compensation_required_by_claimant",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_moral_compensation_required_by_claimant"
        return amount

    def money_penalty_set_by_court(self, amount):
        db_key = db.new_key("money")
        grammar_key = "money_penalty_set_by_court"

        doc_key = db_key + "::" + grammar_key
        db.doc[doc_key] = {
            "_key": db_key,
            "_collection": "money",
            "type": "money_penalty_set_by_court",
            "amount": amount
        }
        db[db_key] = db.doc[doc_key]
        db.doc[doc_key]["_doc_key"] = "money_penalty_set_by_court"
        return amount

    def part_1_of_decision(self, part_1_of_decision):
        tmp_values["decision_title"] = part_1_of_decision
        return part_1_of_decision

    def part_2_of_decision(self, part_2_of_decision):
        tmp_values["decision_title"] += (" " + part_2_of_decision)
        return part_2_of_decision

    def part_3_of_decision(self, part_3_of_decision):
        tmp_values["decision_title"] += (" " + part_3_of_decision)
        return part_3_of_decision

    def part_1_of_topic(self, part_1_of_topic):
        tmp_values["topic"] = part_1_of_topic
        return part_1_of_topic

    def part_2_of_topic(self, part_2_of_topic):
        tmp_values["topic"] += (" " + part_2_of_topic)
        return part_2_of_topic

    def part_3_of_topic(self, part_3_of_topic):
        tmp_values["topic"] += (" " + part_3_of_topic)
        return part_3_of_topic

    def annotation(self, annotation):
        tmp_values["annotation"] = annotation
        return ""


if __name__ == '__main__':
    # rst_to_graph()
    pass
