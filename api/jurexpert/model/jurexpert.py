from kt import TokyoTyrant, TT_TABLE, QueryBuilder, constants
import os 

class Jurexpert(TokyoTyrant):
    '''
    doc is a set of data parsed from one juridical document.
    '''
    doc = {}
    prepared_edges = {}

    def __init__(self):
        super().__init__(serializer=TT_TABLE)
        self.db = self
        self.doc = {}
        self.grammar_path = "{}/api/jurexpert/parser/".format(os.environ["smevna_root_path"])
        self.rst_path = "{}/api/jurexpert/data_rst/".format(os.environ["smevna_root_path"])
        self.grammar = self.grammar_path + "grammar.lark"

    '''
    Graph schema. When modify the schema you MUST test it
    by adding appropriative test, using /api/pytest tests.py
    '''
    schema_graph = [
        "(court) assigns (judge) makes (decision) is_part_of (case) is_categorized_as (category)",
        "(party) represented_by (counsel) makes (doc) is_part_of (case)",
        "(party) makes (doc)",
        "(decision) takes_amount (money)",
        "(decision) dated (date)",
        "(decision) is_matter_of (topic)",
        "(doc) dated (date)",
        "(court) is_located_in (city) is_located_in (area)",
        "(money) defined_with (decision)",
        "(decision) has_body (content)"
    ]

    schema_doc = [
        "(claimant) represented_by (counsel_of_claimant)",
        "(defendant) represented_by (counsel_of_defendant)",
        "(third_party) represented_by (counsel_of_third_party)",
        "(judge) makes (decision)",
        "(decision) dated (date_of_decision)",
        "(court) is_located_in (court_city)",
        "(city) is_located_in (court_area)",
        "(court) assigns (judge)",
        "(decision) is_part_of (case)",
        "(decision) is_matter_of (topic)",
        "(case) is_categorized_as (category)",
        "(money_base_required_by_claimant) defined_with (decision)",
        "(money_base_set_by_court) defined_with (decision)",
        "(money_moral_compensation_required_by_claimant) defined_with (decision)",
        "(money_moral_compensation_set_by_court) defined_with (decision)",
        "(money_penalty_required_by_claimant) defined_with (decision)",
        "(money_penalty_set_by_court) defined_with (decision)",
        "(decision) has_body (content)"
    ]

    def allowed_by_schema(self, s=None, p=None, o=None, schema=None):
        '''
        Define what triplets are allowed for the given subject/predicate/object
        ---- The following returns vertexes ----
        (s='judge', p='makes') => ['decision', 'doc']
        (p='makes', o='doc') => ['judge', 'counsel', 'party']
        (p='makes') => ['judge', 'counsel', 'party', 'doc', 'decision']
        ---- The following returns edges ----
        (s='judge', o='decision') => ['makes']
        (s='court') => ['assigns', 'is_located_in']
        (o='money') => ['takes_amount']
        '''
        if schema is None:
            print("allowed_by_schema(schema=?) - schema is not defined!")
            return
        allowed = []
        # found_triplet = 0
        for branch in schema:
            branch_items = branch.split(" ")
            i = 1
            while(i < (len(branch_items) - 1)):
                if s and p:
                    if branch_items[i - 1][1: -1] == s and branch_items[i] == p:
                        allowed.append(branch_items[i + 1][1: -1])
                elif s and o:
                    if branch_items[i - 1][1: -1] == s and branch_items[i + 1][1: -1] == o:
                        allowed.append(branch_items[i])
                elif p and o:
                    if branch_items[i] == p and branch_items[i + 1][1: -1] == o:
                        allowed.append(branch_items[i - 1][1: -1])
                elif p:
                    if branch_items[i] == p:
                        allowed.append(branch_items[i - 1][1: -1])
                        allowed.append(branch_items[i + 1][1: -1])
                elif s:
                    if branch_items[i - 1][1: -1] == s:
                        allowed.append(branch_items[i])
                elif o:
                    if branch_items[i + 1][1: -1] == o:
                        allowed.append(branch_items[i])
                i += 2
        return allowed

    def contains(self, collection, attr, value):
        '''
        Check if the record already contains in DB
        Return record if founded or None
        '''
        if isinstance(attr, tuple) and isinstance(value, tuple):
            query = QueryBuilder()

            for i in range(len(attr)):
                print("+++ ATTR " + attr[i])
                print("+++ value " + value[i])
                query._conditions.append((attr[i], constants.OP_STR_EQ, value[i]))
            query._limit = 1

            found = query.execute(self)
            print("+++ found", found)
            print("/")
        else:
            query = QueryBuilder() \
                .filter(attr, constants.OP_STR_EQ, value) \
                .limit(1)
            found = query.execute(self)
        return self.db.get(found[0]) if len(found) else None

    def date_key(self, y, m, d):
        return "date.{0}{1}{2}".format(str(y), str(m).zfill(2), str(d).zfill(2))

    def new_key(self, collection=None):
        '''
        Return minimal unused key for the collection, e.g.:
        judge -> judge.00000a
        court -> court.00abe1
        This allow to insert new key/value record safely e.g.:
        ::
            Vertex["judge.00abe1"] = {"name": "Dr. John Smith"}

        The numeric part of the return is in hexadecimal format (without leading 0x)
        '''
        existed_keys = self.match_prefix(collection + ".")
        if len(existed_keys) == 0:
            out = collection + ".000001"
        else:
            try:
                obj_id_max = existed_keys[len(existed_keys) - 1].split(".")[1]
                out = collection + "." + hex(int(obj_id_max, 16) + 1)[2:].zfill(6)
            except Exception:
                print("Collection '{0}' doesn't have numeric id format like {0}.000001, etc.".format(collection))
                out = False
        return out