# Hexastore.
import itertools
import json


class _VariableGenerator(object):
    def __getattr__(self, name):
        return Variable(name)

    def __call__(self, name):
        return Variable(name)


class Hexastore(object):

    def __init__(self, database, prefix='', serialize=json.dumps,
                 deserialize=json.loads):
        self.database = database
        self.prefix = prefix
        self.serialize = serialize
        self.deserialize = deserialize
        self.v = _VariableGenerator()

    def _data_for_storage(self, s, p, o):
        serialized = self.serialize({
            's': s,
            'p': p,
            'o': o})

        # print("SER____ ", dict(serialized))
        '''
        data = {}
        for key in self.keys_for_values(s, p, o):
            data[key] = serialized
        '''

        return self.deserialize(serialized)

    def store(self, s, p, o):
        keys = self.keys_for_values(s, p, o)
        for key in keys:
            self.database[key] = self._data_for_storage(s, p, o)
        return True

    def store_many(self, items):
        data = {}
        for item in items:
            data.update(self._data_for_storage(*item))
        return self.database.update(data)

    def delete(self, s, p, o):
        for key in self.keys_for_values(s, p, o):
            del self.database[key]

    def keys_for_values(self, s, p, o):
        zipped = zip('spo', (s, p, o))
        for ((p1, v1), (p2, v2), (p3, v3)) in itertools.permutations(zipped):
            yield '::'.join((
                ''.join((p1, p2, p3)),
                v1,
                v2,
                v3))

    def keys_for_query(self, s=None, p=None, o=None):
        parts = []
        key = lambda parts: '::'.join(parts)

        if s and p and o:
            parts.extend(('spo', s, p, o))
        elif s and p:
            parts.extend(('spo', s, p))
        elif s and o:
            parts.extend(('sop', s, o))
        elif p and o:
            parts.extend(('pos', p, o))
        elif s:
            parts.extend(('spo', s))
        elif p:
            parts.extend(('pso', p))
        elif o:
            parts.extend(('osp', o))
        return key(parts)

    def query(self, s=None, p=None, o=None):
        start = self.keys_for_query(s, p, o)
        try:
            # TODO
            # We have to relize 'yield" function
            # in order to return iterator, not array
            # It may be done via Lua as I guess..
            return self.database.match_regex("^" + start)
        except KeyError:
            raise StopIteration
        pass
    '''
    def query_many(self, query={'s': None, 'p': None, 'o': None}):
        result = []
        q = query.copy()
        if isinstance(s, set):
            for item in s:
                q["s"] = item
                result.append(self.query(s=si, p, o))
    '''
    def v(self, name):
        return Variable(name)

    def search(self, *conditions):
        results = {}  # e.g.: { <Variable X>: {"decision.000001", "decision.000005"}}

        for condition in conditions:
            result_list = {}  # e.g.: {"spo::decision.000001::dated::date.20170227": {"s": "decision.000001", "p": "dated", "o": "date.20170227"}}
            if isinstance(condition, tuple):
                query = dict(zip('spo', condition))
            else:
                query = condition.copy()
            materialized = {}
            targets = []

            for part in ('s', 'p', 'o'):
                if isinstance(query[part], Variable):
                    variable = {"part": "", "var": None}
                    variable["part"] = part
                    variable["var"] = query.pop(part)
                    targets.append((variable["var"], variable["part"]))
                    materialized[part] = set()

            # We use the result values from a previous condition and do O(results)
            # loops looking for a single variable.
            if variable["var"] in results:
                for each_key in results[variable["var"]]:
                    query[variable["part"]] = each_key
                    result_list.update(self.query(**query))
            else:
                result_list = self.query(**query)

            for key in result_list:
                for var, part in targets:
                    materialized[part].add(result_list[key][part])

            for var, part in targets:
                if var in results:
                    results[var] &= materialized[part]
                else:
                    results[var] = materialized[part]

        return dict((var.name, vals) for (var, vals) in results.items())


class Variable(object):
    __slots__ = ['name']

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Variable: %s>' % (self.name)
