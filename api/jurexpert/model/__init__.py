import os
from kt import TokyoTyrant, TT_TABLE
tt = TokyoTyrant(serializer=TT_TABLE)

data = {
    'huey': {'name': 'huey', 'type': 'cat', 'eyes': 'blue', 'age': '7'},
    'mickey': {'name': 'mickey', 'type': 'dog', 'eyes': 'blue', 'age': '9'},
    'zaizee': {'name': 'zaizee', 'type': 'cat', 'eyes': 'blue', 'age': '5'},
    'charlie': {'name': 'charlie', 'type': 'human', 'eyes': 'brown', 'age': '35'},
    'leslie': {'name': 'leslie', 'type': 'human', 'eyes': 'blue', 'age': '34'},
    'connor': {'name': 'connor', 'type': 'human', 'eyes': 'brown', 'age': '3'},
}

