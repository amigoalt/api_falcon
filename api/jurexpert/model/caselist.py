import ujson
import time
import locale
locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')

from peewee import fn
from kt import TokyoTyrant, TT_TABLE, QueryBuilder, constants
from datetime import datetime
from api.jurexpert.model.graph import Hexastore
from api.jurexpert.model.jurexpert import Jurexpert

from api import log

db = Jurexpert()
graph = Hexastore(database=db)


def decision_content(des_key=None):
    out = ""
    # print("HERE ==========")
    # print(TokyoTyrant().script(name="set_updated"))
    if des_key is not None:
        query = (QueryBuilder() \
            .filter("_collection", constants.OP_STR_EQ, "content")
            .filter("head_key", constants.OP_STR_EQ, des_key))
        rows = query.get(db)
        out = rows[0][1]["body"]
    return out


def list_of_decisions(year=None, limit=10, offset=0):
    out = []
    if year is None:
        query = (QueryBuilder() \
            .filter("_collection", constants.OP_STR_EQ, "decision")
            .order_by("_date", constants.ORDER_STR_DESC)
            .limit(limit)
            .offset(offset))
    else:
        query = (QueryBuilder() \
            .filter("_collection", constants.OP_STR_EQ, "decision")
            .filter("_date", constants.OP_STR_STARTSWITH, year)
            .order_by("_date", constants.ORDER_STR_DESC)
            .limit(limit)
            .offset(offset))
    decisions = query.get(db)
    # Make date formatting for human
    # Add extra fields: topic, annotation, case number
    for des in decisions:
        t = (int(des[1]["_date"][0:4]), int(des[1]["_date"][4:6]), int(des[1]["_date"][6:8]), 0, 0, 0, 0, 0, 0)
        t = time.mktime(t)
        des[1]["_date"] = time.strftime("%b %d, %Y", time.localtime(t)).title()
        des[1].update(decision_extra_fields(des[0]))
        out.append(des[1])

    return out


def decision_extra_fields(key="decision.000001"):
    '''
    Compound extra fields (topic, annotation, case_number)
    It is used to show list of decisions as table in Practice page
    '''
    row = {}

    X = graph.v.X
    topics = list(graph.search((key, "is_matter_of", X))['X'])
    cases = list(graph.search((key, "is_part_of", X))['X'])

    annotation = db[key]['annotation'] if db[key]['annotation'] else None

    if len(cases) > 1:
        log.warn("{0} is_part_of more than one Case: {1}".format(key, cases))
    elif len(cases) == 0:
        log.warn("{0} is_part_of none of Case".format(key))
    else:
        case_key = cases[0]
        topic_key = topics[0] if len(topics) > 0 else ""
        if topic_key and db[topic_key]:
            row.update({
                "topic": db[topic_key]["title"],
                })
        if db[key] and db[key]["case_key"] and db[case_key]:
            row.update({
                "case_number": db[case_key]["number"],
                })
        if annotation is not None:
            row.update({
                "annotation": annotation
            })
    return row
