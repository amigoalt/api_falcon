import ujson
import os
import falcon
from falcon_pagination.offset_pagination_hook import OffsetPaginationHook
from docutils.core import publish_parts, publish_doctree, publish_from_doctree
from api.jurexpert.model.caselist import list_of_decisions, decision_content, decision_extra_fields

from api.jurexpert.model.jurexpert import Jurexpert
db = Jurexpert()


class Parsing:
    def on_get(self, req, resp):
        ''' URI example: /jurexpert/parse '''
        from .parser.rst_to_graph import rst_to_graph
        rst_text = rst_to_graph()
        out = {"decisions": list_of_decisions(year=None, limit=0, offset=0)}
        resp.body = ujson.dumps(out)
        resp.status = falcon.HTTP_200


class Decision:
    @falcon.before(OffsetPaginationHook())
    def on_get(self, req, resp):
        ''' URI example: /jurexpert/decision/list '''
        p_limit = req.context.pagination['limit']
        p_ofset = req.context.pagination['offset']
        if p_ofset < 1:
            p_ofset = 0


        out = {"decisions": list_of_decisions(year=None, limit=p_limit, offset=p_ofset)}
        resp.body = ujson.dumps(out)

        resp.status = falcon.HTTP_200


class Content:
    def on_get(self, req, resp, des_key):
        extra = decision_extra_fields(des_key)
        extra["annotation"] = publish_parts(extra["annotation"], writer_name='html')['html_body'] 
        title = db[des_key]["title"] if len(db[des_key]["title"]) > 0 else 'Дело {0} {1}'.format(extra["case_number"], extra["topic"])
        out = {
            "article": {
                "body": decision_content(des_key),
                "title": title,
                "html": "",
            }
        }
        out["article"].update(extra)
        # resp.body = ujson.dumps(out)

        article = out["article"]
        # if html is empty, use rst/markdown from "content" field
        # For config options see:
        # https://docutils.sourceforge.io/docs/user/config.html
        overrides = {
            'input_encoding': 'unicode',
            'doctitle_xform': False,
            'initial_header_level': 2,
            'toc_backlinks': 'none',
            'table_style': 'colwidths-auto',
            'embed_stylesheet': False,
            'template': '{}/api/view/custom_docutils_template.txt'.format(os.environ["smevna_root_path"])
        }

        # To render Table of content in separate <div>
        # use this code
        main_div_start = publish_doctree('.. raw:: html\n\n   <div class="page-body">').document.children[0]
        toc_div_start = publish_doctree('.. raw:: html\n\n   <div class="page-toc">').document.children[0]
        div_end = publish_doctree('.. raw:: html\n\n   </div>').document.children[0]

        doctree = publish_doctree(article["body"])
        doc_nodes = doctree.document.children

        # get <topic> elements
        topic_nodes = list(filter(lambda t: t.tagname == 'topic', doc_nodes))

        if len(topic_nodes) > 0:
            # add <div class="separate-div-right" for topics and </div> too
            topic_nodes.insert(0, toc_div_start)
            topic_nodes.append(div_end)

            # remove the topics from initial doctree
            doc_nodes = list(filter(lambda n: n.tagname != 'topic', doc_nodes))

            # add <div class="main" for content and </div> too
            doc_nodes.insert(0, main_div_start)
            doc_nodes.append(div_end)

            # and return modified doctree
            doc_nodes = topic_nodes + doc_nodes
            doctree.document.children = doc_nodes

        out["article"]["html"] = publish_from_doctree(
            doctree,
            writer_name='html',
            settings_overrides=overrides
        )
        resp.body = ujson.dumps(out)
        resp.status = falcon.HTTP_200
