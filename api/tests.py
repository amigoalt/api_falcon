import pytest
from api.jurexpert.parser.rst_to_graph import Jurexpert


def test_allowed_by_schema():
    jxp = Jurexpert()
    schema_graph = jxp.schema_graph
    assert set(jxp.allowed_by_schema(p="makes", schema=schema_graph)) == set(['judge', 'counsel', 'party', 'doc', 'decision'])
    assert set(jxp.allowed_by_schema(p="assigns", schema=schema_graph)) == set(['court', 'judge'])
    assert set(jxp.allowed_by_schema(p="represented_by", schema=schema_graph)) == set(['party', 'counsel'])
    assert set(jxp.allowed_by_schema(p="is_part_of", schema=schema_graph)) == set(['doc', 'case', 'decision'])
    assert set(jxp.allowed_by_schema(p="takes_amount", schema=schema_graph)) == set(['money', 'decision'])
    assert set(jxp.allowed_by_schema(p="dated", schema=schema_graph)) == set(['date', 'decision', 'doc'])
    assert set(jxp.allowed_by_schema(p="is_located_in", schema=schema_graph)) == set(['court', 'city', 'area'])

    assert set(jxp.allowed_by_schema(s="court", schema=schema_graph)) == set(['assigns', 'is_located_in'])
    assert set(jxp.allowed_by_schema(s="city", schema=schema_graph)) == set(['is_located_in'])
    assert set(jxp.allowed_by_schema(s="judge", schema=schema_graph)) == set(['makes'])
    assert set(jxp.allowed_by_schema(s="party", schema=schema_graph)) == set(['makes', 'represented_by'])
    assert set(jxp.allowed_by_schema(s="counsel", schema=schema_graph)) == set(['makes'])
    assert set(jxp.allowed_by_schema(s="decision", schema=schema_graph)) == set(['dated', 'takes_amount', 'is_part_of'])
    assert set(jxp.allowed_by_schema(s="case", schema=schema_graph)) == set(['is_categorized_as'])
    assert set(jxp.allowed_by_schema(s="doc", schema=schema_graph)) == set(['dated', 'is_part_of'])

    assert set(jxp.allowed_by_schema(o="judge", schema=schema_graph)) == set(['assigns'])
    assert set(jxp.allowed_by_schema(o="city", schema=schema_graph)) == set(['is_located_in'])
    assert set(jxp.allowed_by_schema(o="counsel", schema=schema_graph)) == set(['represented_by'])
    assert set(jxp.allowed_by_schema(o="decision", schema=schema_graph)) == set(['makes'])
    assert set(jxp.allowed_by_schema(o="case", schema=schema_graph)) == set(['is_part_of'])
    assert set(jxp.allowed_by_schema(o="doc", schema=schema_graph)) == set(['makes'])
    assert set(jxp.allowed_by_schema(o="date", schema=schema_graph)) == set(['dated'])
    assert set(jxp.allowed_by_schema(o="category", schema=schema_graph)) == set(['is_categorized_as'])
    assert set(jxp.allowed_by_schema(o="money", schema=schema_graph)) == set(['takes_amount'])
    assert set(jxp.allowed_by_schema(o="area", schema=schema_graph)) == set(['is_located_in'])

    assert set(jxp.allowed_by_schema(s="court", p="is_located_in", schema=schema_graph)) == set(['city'])
    assert set(jxp.allowed_by_schema(s="city", p="is_located_in", schema=schema_graph)) == set(['area'])
    assert set(jxp.allowed_by_schema(s="court", p="assigns", schema=schema_graph)) == set(['judge'])
    assert set(jxp.allowed_by_schema(s="judge", p="makes", schema=schema_graph)) == set(['decision'])
    assert set(jxp.allowed_by_schema(s="party", p="makes", schema=schema_graph)) == set(['doc'])
    assert set(jxp.allowed_by_schema(s="counsel", p="makes", schema=schema_graph)) == set(['doc'])
    assert set(jxp.allowed_by_schema(s="decision", p="is_part_of", schema=schema_graph)) == set(['case'])
    assert set(jxp.allowed_by_schema(s="decision", p="takes_amount", schema=schema_graph)) == set(['money'])
    assert set(jxp.allowed_by_schema(s="decision", p="dated", schema=schema_graph)) == set(['date'])
    assert set(jxp.allowed_by_schema(s="case", p="is_categorized_as", schema=schema_graph)) == set(['category'])
    assert set(jxp.allowed_by_schema(s="party", p="represented_by", schema=schema_graph)) == set(['counsel'])
    assert set(jxp.allowed_by_schema(s="doc", p="is_part_of", schema=schema_graph)) == set(['case'])
    assert set(jxp.allowed_by_schema(s="city", p="is_located_in", schema=schema_graph)) == set(['area'])

    assert set(jxp.allowed_by_schema(p="is_located_in", o="city", schema=schema_graph)) == set(['court'])
    assert set(jxp.allowed_by_schema(p="is_located_in", o="area", schema=schema_graph)) == set(['city'])
    assert set(jxp.allowed_by_schema(p="assigns", o="judge", schema=schema_graph)) == set(['court'])
    assert set(jxp.allowed_by_schema(p="makes", o="decision", schema=schema_graph)) == set(['judge'])
    assert set(jxp.allowed_by_schema(p="makes", o="doc", schema=schema_graph)) == set(['counsel', 'party'])
    assert set(jxp.allowed_by_schema(p="is_part_of", o="case", schema=schema_graph)) == set(['decision', 'doc'])
    assert set(jxp.allowed_by_schema(p="takes_amount", o="money", schema=schema_graph)) == set(['decision'])
    assert set(jxp.allowed_by_schema(p="dated", o="date", schema=schema_graph)) == set(['decision', 'doc'])
    assert set(jxp.allowed_by_schema(p="is_categorized_as", o="category", schema=schema_graph)) == set(['case'])
    assert set(jxp.allowed_by_schema(p="represented_by", o="counsel", schema=schema_graph)) == set(['party'])


def test_contains():
    jxp = Jurexpert()
    assert jxp.contains("decision", ("title", "case_number"), ("В удовлетворении искового заявления отказать", "2-3-40/2017")) == {'_key': 'decision.000001', '_collection': 'decision', 'title': 'В удовлетворении искового заявления отказать', 'case_number': '2-3-40/2017'}
    assert jxp.contains("case", "number", "2-3-40/2017") == {'_key': 'case.000001', '_collection': 'case', 'number': '2-3-40/2017'}
