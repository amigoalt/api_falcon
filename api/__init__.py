import os
import logging, logging.config

logging.config.fileConfig(os.environ['smevna_root_path'] + '/log/log.conf')
log=logging.getLogger('main')