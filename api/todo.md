* [x] Spell checking
* [x] Testing with FireFox (Practice page onclick problem)
* [x] About page (Update right column)
* [x] Change Slide on "Practice" page
* [x] Testing unde mobile device (see "Practice" page with Smartphone)
* [x] Fix "Error 500" for Case № 2-335/2016
* [x] Add Email to Contact page
* [x] TOC on "Decision" page doesn't work properly (localhost#2 instead localhost/practice/decision.000003#2)
* [ ] Tailwind styles saved in DB problem. When Tailwind class is written in the DB content, then CSS compiler can not make it into build. E.g. "Contact" page: the photo soesn't fit its place correctlty.