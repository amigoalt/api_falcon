import re
import falcon
import peewee as pw
from transliterate import translit
from .database import db
from playhouse.shortcuts import model_to_dict, dict_to_model, update_model_from_dict
import ujson
import uuid
import bcrypt
from falcon_pagination.offset_pagination_hook import OffsetPaginationHook


# def init_tables():
#    db.create_tables([Rule, User, Tag, Dialect, Article, Menu, ArticleTag, MenuTag], safe=True)


class BaseModel(pw.Model):
    class Meta:
        database = db


class Rule(BaseModel):
    name = pw.CharField()


class User(BaseModel):
    name = pw.CharField(unique=True)
    email = pw.CharField(primary_key=True)
    username = pw.CharField(unique=True)
    password = pw.CharField()
    rule = pw.ForeignKeyField(Rule, backref='users')
    token = pw.CharField(null=True)

    def check_password(self, password):
        hashed = self.password
        return bcrypt.checkpw(password.encode('UTF-8'), hashed.encode('UTF-8'))

    @staticmethod
    def _make_token(user):
        user.token = uuid.uuid4().hex
        user.save()
        return user.token

    def check_token(self):
        pass

    @staticmethod
    def login(username, password):
        try:
            user = User.get(User.username == username)
            if user.check_password(password):
                token = User._make_token(user)
                return {'token': token}
            else:
                raise pw.DoesNotExist
        except pw.DoesNotExist:
            return {'error': 'No user found with that credentials'}
        except pw.IntegrityError as px:
            return {'error': '{}'.format(str(px))}

    @staticmethod
    def logout(token):
        try:
            user = User.get(User.token == token)
            user.token = 'null'
            user.save()
            return {'message': 'You have logged out seccessful!'}
        except pw.DoesNotExist:
            return {'error': 'Token not found. Log out error.'}
        except pw.IntegrityError as px:
            return {'error': '{}'.format(str(px))}


class Tag(BaseModel):
    name = pw.CharField(unique=True)


class Dialect(BaseModel):
    name = pw.CharField(unique=True)


class Menu(BaseModel):
    title = pw.CharField()
    order = pw.IntegerField(default=0)
    slug = pw.CharField(default='')


class MenuTag(BaseModel):
    menu_id = pw.ForeignKeyField(Menu, backref='menutag', on_delete='CASCADE')
    tag_id = pw.ForeignKeyField(Tag, backref='menutag', on_delete='CASCADE')


class Article(BaseModel):
    title = pw.CharField(unique=False)
    annotation = pw.TextField(default='')
    content = pw.TextField(default='')
    html = pw.TextField(default='')
    slug = pw.TextField(unique=True, null=True)
    # dialect = pw.ForeignKeyField(Dialect, backref='articles', null=True, on_delete='SET NULL')
    # user = pw.ForeignKeyField(User, backref='articles', null=True, on_delete='SET NULL')

    dialect = pw.ForeignKeyField(Dialect, backref='articles', null=True, on_delete='SET NULL', on_update='CASCADE')
    user = pw.ForeignKeyField(User, backref='articles', null=True, on_delete='SET NULL', on_update='CASCADE')

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = translit(self.title, 'ru', reversed=True)
            slug = re.sub('[^\w]+', '-', slug.lower())
            self.slug = slug

        ret = super().save(*args, **kwargs)
        return ret


    # TODO Add def insert() to make slug automatically when post new article via API


class ArticleTag(BaseModel):
    article_id = pw.ForeignKeyField(Article, backref='articletag', on_delete='CASCADE')
    tag_id = pw.ForeignKeyField(Tag, backref='articletag', on_delete='CASCADE')
