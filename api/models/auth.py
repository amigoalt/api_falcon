import falcon
from api.models.db import User
import peewee as pw


class AuthMiddleware(object):
    def process_request(self, req, resp):
        if(req.method in ["POST", "PUT", "DELETE", "PATCH"]):
            if req.path != '/login':
                token = req.get_header('Authorization')

                challenges = ['Token type="Fernet"']

                if token is None:
                    description = (
                        'Please provide an auth token as part of the request.'
                    )

                    raise falcon.HTTPUnauthorized(
                        'Auth token required',
                        description,
                        challenges
                    )

                if not self._token_is_valid(token):
                    description = (
                        'The provided auth token is not valid. Please request a new token and try again.'
                    )

                    raise falcon.HTTPUnauthorized(
                        'Authentication required',
                        description,
                        challenges
                    )

    def _token_is_valid(self, token):
        token = token.split(' ')
        if token[0] == 'Token':
            try:
                User.get(User.token == token[1])
                return True
            except pw.DoesNotExist:
                return False
        else:
            return False


class CORSComponent(object):
    def process_response(self, req, resp, resource, req_succeeded):
        resp.set_header('Access-Control-Allow-Origin', '*')
        # TODO Make CORS enabled.
        # resp.set_header('Access-Control-Allow-Credentials', 'true')
        # resp.set_header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
        # resp.set_header('Access-Control-Allow-Headers', 'X-Requested-With, content-type')

        if (req_succeeded
            and (req.method == 'OPTIONS' or req.method == 'PATCH' or req.method == 'PUT' or req.method == 'POST')
            and req.get_header('Access-Control-Request-Method')):
            # NOTE(kgriffs): This is a CORS preflight request. Patch the
            #   response accordingly.

            allow = resp.get_header('Allow')
            resp.delete_header('Allow')

            allow_headers = req.get_header(
                'Access-Control-Request-Headers',
                default='*'
            )

            resp.set_headers((
                ('Access-Control-Allow-Methods', allow),
                ('Access-Control-Allow-Headers', allow_headers),
                ('Access-Control-Max-Age', '86400'),  # 24 hours
            ))
