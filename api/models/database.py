import peewee as pw

db = pw.SqliteDatabase('blog.db')
db.pragma('foreign_keys', 1, permanent=True)