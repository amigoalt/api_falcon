import falcon as falcon
from api.models.db import Rule, Tag, Dialect
from api.models.auth import AuthMiddleware, CORSComponent
from api.models.serialize import RequireJSON

from api.resources.users import Users
from api.resources.articles import Articles, ArticleTags, ArticleTagsMark, ArticleTagsUnmark
from api.resources.menus import Menus, MenuTags, MenuTagsMark, MenuTagsUnmark
from api.resources.tags import Tags

from api.jurexpert.resources import Decision, Content, Parsing


'''
from falcon_cors import CORS

cors = CORS(
    allow_all_origins=True,
    allow_all_headers=True,
    allow_all_methods=True,
    allow_credentials_all_origins=True
)
'''


app = falcon.API(middleware=[
    CORSComponent(),
    AuthMiddleware()]
)

'''
app = falcon.API(middleware=[
    cors.middleware]
    # AuthMiddleware()
    # RequireJSON()]
)
'''

app.req_options.auto_parse_form_urlencoded = True
app.add_route('/users', Users())
app.add_route('/login', Users())
app.add_route('/logout', Users())

app.add_route('/articles', Articles())
app.add_route('/articles/{id_or_slug}', Articles())
app.add_route('/articles/{id_or_slug}/tags', ArticleTags())
app.add_route('/articles/{id_or_slug}/tags/mark', ArticleTagsMark())
app.add_route('/articles/{id_or_slug}/tags/{tag_id}/unmark', ArticleTagsUnmark())

app.add_route('/tags/{id}', Tags())
app.add_route('/tags', Tags())
app.add_route('/rules', Rule())
app.add_route('/dialects', Dialect())

app.add_route('/menus', Menus())
app.add_route('/menus/{id}', Menus())
app.add_route('/menus/{id}/tags', MenuTags())
app.add_route('/menus/{id}/tags/{tag_id}/mark', MenuTagsMark())
app.add_route('/menus/{id}/tags/{tag_id}/unmark', MenuTagsUnmark())

# Jurexpert API
app.add_route('/jurexpert/parse', Parsing())
app.add_route('/jurexpert/decision/list', Decision())
app.add_route('/jurexpert/decision/{des_key}', Content())
