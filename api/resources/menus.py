import ujson
import falcon
import peewee as pw
from playhouse.shortcuts import model_to_dict, dict_to_model, update_model_from_dict
from api.models.db import Menu, MenuTag, Tag


class Menus:
    def on_get(self, req, resp, id=0):
        try:
            menus = Menu.select().order_by(Menu.order)
            out = {"menus": [model_to_dict(menu) for menu in menus]}
            resp.body = ujson.dumps(out)
        except Exception as ex:
            resp.body = ujson.dumps({"error": ex})

    def on_post(self, req, resp, id=0):
        if (int(id) > 0):
            resp.body = ujson.dumps({"error": "POST method is not allowed.  To update menu item use PATCH."})
            resp.status = falcon.HTTP_200
        elif (req.media['menu']):
            try:
                menu_item = req.media['menu']
                menu_id = Menu.insert([menu_item]).execute()
                resp.body = ujson.dumps({
                    "mesage": "Menu '{}' has been added with id: {}."
                    .format(menu_item['title'], menu_id)})
            except Exception as ex:
                resp.body = ujson.dumps({"error": str(ex)})
        else:
            resp.body = ujson.dumps({"error": "No 'menu' node exist in the POST body."})
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, id):
        try:
            if Menu.delete().where(Menu.id == id).execute():
                resp.body = ujson.dumps({"message": "Menu [{}] has been deleted succsessfully.".format(id)})
            else:
                resp.body = ujson.dumps({"error": "Menu [{}] doesn't exist.".format(id)})
        except Exception as px:
            resp.body = ujson.dumps({'error': str(px)})
        resp.status = falcon.HTTP_200

    def on_patch(self, req, resp, id):
        if 'menu' in req.media.keys():
            try:
                menu_item = Menu.get(Menu.id == id)
                update_model_from_dict(menu_item, req.media["menu"])
                menu_item.save()
                resp.body = ujson.dumps({"message": "Menu [{}] has been updated.".format(menu_item.id)})
            except pw.DoesNotExist as px:
                resp.body = ujson.dumps({'error': str(px)})
            resp.status = falcon.HTTP_200
        else:
            resp.body = ujson.dumps({"error": "'menu' object has not been found in POST body"})
            resp.status = falcon.HTTP_200


class MenuTags:
    def on_get(self, req, resp, id):
        ''' URI example: /menus/12/tags '''
        # Does Menu exist? If no - Exception and exit
        try:
            menu = Menu.get(Menu.id == id)
            # Are the Menu marked with any tag? If no - exception and exit
            try:
                tags = MenuTag.select().where(MenuTag.menu_id == menu.id)
                resp.body = ujson.dumps({"menu-tags": [model_to_dict(tag_id) for tag_id in tags]})
            except pw.DoesNotExist:
                resp.body = ujson.dumps({"message": "Menu [{}] are not marked with any Tags.".format(menu.title)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Menu id[{}] does not exist.".format(id)})

        resp.status = falcon.HTTP_200


class MenuTagsMark:
    def on_post(self, req, resp, id):
        ''' URI example: /menus/12/tags
            POST body example:
            {
                "tagname": "Интересно"
            }
        '''
        # Does Menu exist? If no - Exception and exit
        try:
            menu = Menu.get(Menu.id == id)
            # Does Tag exist? If no - create Tag or error with exit
            try:
                tag, tag_created = Tag.get_or_create(
                    name=req.media['tagname'],
                    defaults={'name': req.media['tagname']}
                )
                # Has Menu already marked with the Tag? If no - create record in MenuTag table
                try:
                    menu_tag, menu_tag_created = MenuTag.get_or_create(
                        menu_id=id,
                        tag_id=tag.id,
                        defaults={'menu_id': id, 'tag_id': tag.id}
                    )
                    if menu_tag_created:
                        resp.body = ujson.dumps({"message": "Menu [{}] has marked with Tag [{}] successfully!".format(menu.title, tag.name)})
                    else:
                        resp.body = ujson.dumps({"message": "Menu [{}] has already marked with Tag [{}]".format(menu.title, tag.name)})
                except pw.IntegrityError as ex:
                    resp.body = ujson.dumps({"error": str(ex)})
            except pw.IntegrityError as ex:
                resp.body = ujson.dumps({"error": str(ex)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Menu id[{}] does not exist.".format(id)})

        resp.status = falcon.HTTP_200


class MenuTagsUnmark:
    def on_delete(self, req, resp, id, tag_id):
        ''' URI example: /menus/12/tags/4/unmark '''
        # Does Menu exist? If no - Exception and exit
        try:
            menu = Menu.get(Menu.id == id)
            # Does the Tag exist? If no - exception and exit.
            try:
                tag = Tag.get(Tag.id == tag_id)
                # Are Menu marked with the Tag? If no - exception and exit.
                try:
                    menu_tag = MenuTag.get(MenuTag.menu_id == menu.id, MenuTag.tag_id == tag.id)
                    # Unmark menu or exceptio
                    try:
                        n = menu_tag.delete_instance()
                        if n > 0:
                            resp.body = ujson.dumps({"message": "Menu [{}] has been unmarked with tag [{}].".format(menu.title, tag.name)})
                        else:
                            resp.body = ujson.dumps({"error": "Menu [{}] has NOT been unmarked with tag [{}].".format(menu.title, tag.name)})
                    except pw.IntegrityError as ex:
                        resp.body = ujson.dumps({"error": str(ex)})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Menu [{}] are not marked with tag {}.".format(menu.title, tag.name)})
            except pw.DoesNotExist:
                resp.body = ujson.dumps({"message": "Tag id[{}] does not exist.".format(tag_id)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Menu id[{}] does not exist.".format(id)})

        resp.status = falcon.HTTP_200
