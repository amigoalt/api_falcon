import os
from lxml import etree
from io import StringIO
import ujson
import falcon
from falcon_pagination.offset_pagination_hook import OffsetPaginationHook
import peewee as pw
from playhouse.shortcuts import model_to_dict, dict_to_model, update_model_from_dict
from api.models.db import Article, ArticleTag, Tag
from docutils.core import publish_parts, publish_doctree, publish_from_doctree


def find_first_image_in_docutils_tree(tree):
    # good tutorial for lxml:
    # https://www.sites.google.com/site/bmaupinwiki/home/programming/python/python-xml-lxml
    out = None
    xml_text = tree.asdom().toxml()
    root = etree.fromstring(xml_text)
    images = root.xpath(".//image")
    if len(images) > 0:
        out = images[0].attrib['uri']
    return out


class Articles:
    @falcon.before(OffsetPaginationHook())
    def on_get(self, req, resp, id_or_slug=""):
        if(len(id_or_slug) > 0):
            if(id_or_slug.isdigit()):
                id = id_or_slug
                # Getting all content of an Article by id
                # e.g. /articles/12 (id = 12)
                try:
                    article = Article.get(Article.id == id)
                    resp.body = ujson.dumps({"article": model_to_dict(article, recurse=False)})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Article[{}] doesn't exist.".format(id)})
            else:
                # TOC & slug
                # If slug contains query like "?toc=1"
                # then it means that the article should be accompanied with TOC
                if req.get_param("toc"):
                    toc = True
                else:
                    toc = False
                slug = id_or_slug + "?toc=1" if toc else id_or_slug

                # Getting all content of an Article by slug
                # e.g. /articles/slug_string
                try:
                    article = Article.get(Article.slug == slug)
                    # if html is empty, use rst/markdown from "content" field
                    if (len(article.html) == 0):
                        # For config options see:
                        # https://docutils.sourceforge.io/docs/user/config.html
                        overrides = {
                            'input_encoding': 'unicode',
                            'doctitle_xform': False,
                            'initial_header_level': 2,
                            'toc_backlinks': 'none',
                            'table_style': 'colwidths-auto',
                            'embed_stylesheet': False,
                            'template': '{}/api/view/custom_docutils_template.txt'.format(os.environ["smevna_root_path"])
                        }

                        # To render Table of content in separate <div>
                        # use this code
                        main_div_start = publish_doctree('.. raw:: html\n\n   <div class="page-body">').document.children[0]
                        toc_div_start = publish_doctree('.. raw:: html\n\n   <div class="page-toc">').document.children[0]
                        div_end = publish_doctree('.. raw:: html\n\n   </div>').document.children[0]

                        doctree = publish_doctree(article.content)
                        doc_nodes = doctree.document.children

                        # get <topic> elements
                        topic_nodes = list(filter(lambda t: t.tagname == 'topic', doc_nodes))

                        if len(topic_nodes) > 0:
                            # add <div class="separate-div-right" for topics and </div> too
                            topic_nodes.insert(0, toc_div_start)
                            topic_nodes.append(div_end)

                            # remove the topics from initial doctree
                            doc_nodes = list(filter(lambda n: n.tagname != 'topic', doc_nodes))

                            # add <div class="main" for content and </div> too
                            doc_nodes.insert(0, main_div_start)
                            doc_nodes.append(div_end)

                            # and return modified doctree
                            doc_nodes = topic_nodes + doc_nodes
                            doctree.document.children = doc_nodes

                        article.html = publish_from_doctree(
                            doctree,
                            writer_name='html',
                            settings_overrides=overrides
                        )
                    resp.body = ujson.dumps({"article": model_to_dict(article, recurse=False)})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Article[{}] doesn't exist.".format(slug)})

            resp.status = falcon.HTTP_200
        else:
            # Getting list of articles
            # e.g. /articles (no 'id' exists)
            p_limit = req.context.pagination['limit']
            p_ofset = req.context.pagination['offset']
            if p_ofset < 1:
                p_ofset = 1

            # Getting tag names to filter articles by tags
            if "tags" in req.params and len(req.params["tags"]) > 0:
                tags = req.params["tags"].split(',')
            else:
                tags = []

            try:
                if(len(tags) > 0):
                    list_articles = Article.select().join(Tag, on=(Tag.name << tags)).join(ArticleTag, on=(Tag.id == ArticleTag.tag_id)).where(ArticleTag.article_id == Article.id).order_by(Article.id.desc()).paginate(p_ofset, p_limit)
                else:
                    list_articles = Article.select().order_by(Article.id).paginate(p_ofset, p_limit)

                # get first image founded in "content" field
                # it is required to show the images in the list of articles
                arts = [model_to_dict(art, recurse=False) for art in list_articles]
                modified_arts = []
                for article in arts:
                    article['picture'] = ''
                    tree = publish_doctree(article['content'])
                    article['picture'] = find_first_image_in_docutils_tree(tree)
                    modified_arts.append(article)

                out = {"articles": modified_arts}
                resp.body = ujson.dumps(out)
            except pw.DoesNotExist as px:
                resp.body = ujson.dumps({'error': str(px)})
            resp.status = falcon.HTTP_200

    def on_post(self, req, resp, id_or_slug=""):
        if(len(id_or_slug) > 0):
            resp.body = ujson.dumps({"error": "POST method is not allowed.  To update article use PATCH."})
            resp.status = falcon.HTTP_200
        elif 'article' in req.media.keys():
            try:
                article_posted = req.media['article']
                entry_id = Article.insert([article_posted]).execute()

                # make slug
                article = Article.get(Article.id == entry_id)
                article.save()  # slug is generated automatically here

                resp.body = ujson.dumps({"message": "Entry {} '{}' has been added.".format(entry_id, article.title)})
            except Exception as px:
                resp.body = ujson.dumps({'error': str(px)})
        else:
            resp.body = ujson.dumps({"error": "'article' object has not been found in POST body"})

        resp.status = falcon.HTTP_200

    def on_patch(self, req, resp, id_or_slug=""):
        if(len(id_or_slug) > 0):
            if(id_or_slug.isdigit()):
                id = id_or_slug
            else:
                id = None
                slug = id_or_slug
            if 'article' in req.media.keys():
                try:
                    if id is None:
                        article = Article.get(Article.slug == slug)
                    else:
                        article = Article.get(Article.id == id)
                    update_model_from_dict(article, req.media["article"])
                    article.save()
                    resp.body = ujson.dumps({"message": "Article [{}] has been updated.".format(article.id)})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Article[{}] doesn't exist.".format(id)})
                except pw.IntegrityError as px:
                    resp.body = ujson.dumps({'error': str(px)})
                except Exception as px:
                    resp.body = ujson.dumps({'error': str(px)})
                resp.status = falcon.HTTP_200
            else:
                resp.body = ujson.dumps({"error": "'article' object has not been found in PATCH body"})
                resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, id_or_slug=""):
        if(len(id_or_slug) > 0):
            if(id_or_slug.isdigit()):
                id = id_or_slug
            else:
                id = None
                slug = id_or_slug
        try:
            if id is None:
                if Article.delete().where(Article.slug == slug).execute():
                    resp.body = ujson.dumps({"message": "Article slug[{}] has been deleted succsessfully.".format(slug)})
            else:
                if Article.delete().where(Article.id == id).execute():
                    resp.body = ujson.dumps({"message": "Article id[{}] has been deleted succsessfully.".format(id)})

        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Article[{}] doesn't exist.".format(id)})
        except pw.IntegrityError as px:
            resp.body = ujson.dumps({'error': str(px)})
        except Exception as px:
            resp.body = ujson.dumps({'error': str(px)})

        resp.status = falcon.HTTP_200


class ArticleTags:
    def on_get(self, req, resp, id_or_slug):
        ''' URI example: /articles/12/tags '''
        # Does article exist? If no - Exception and exit
        try:
            article = Article.get(Article.id == id_or_slug)
            # Are the Article marked with any tag? If no - exception and exit
            try:
                tags = ArticleTag.select().where(ArticleTag.article_id == article.id)
                resp.body = ujson.dumps({"article-tags": [model_to_dict(tag_id, recurse=False) for tag_id in tags]})
            except pw.DoesNotExist:
                resp.body = ujson.dumps({"message": "Article [{}] are not marked with any Tags.".format(article.title)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Article id[{}] does not exist.".format(id_or_slug)})

        resp.status = falcon.HTTP_200


'''
class ArticlesByTags:
    def on_get(self, req, resp, tags_list):
        URI example: /articles/tags/blog,javascript,sveltejs
        if(len(tags_list) > 0):
            tags = tags_list.split(",")
            try:
                list_articles = Article.select().join(Tag).where(Tag.name << tags).order_by(Article.id).paginate(p_ofset, p_limit)

            # Does article exist? If no - Exception and exit
            try:
                article = Article.get(Article.id == id)
                # Are the Article marked with any tag? If no - exception and exit
                try:
                    tags = ArticleTag.select().where(ArticleTag.article_id == article.id)
                    resp.body = ujson.dumps({"article-tags": [model_to_dict(tag_id, recurse=False) for tag_id in tags]})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Article [{}] are not marked with any Tags.".format(article.title)})
            except pw.DoesNotExist:
                resp.body = ujson.dumps({"message": "Article id[{}] does not exist.".format(id)})

        resp.status = falcon.HTTP_200
'''


class ArticleTagsMark:
    def on_post(self, req, resp, id_or_slug):
        ''' URI example: /articles/12/tags/mark
            POST body example:
            {
                "tagname": "Интересно"
            }
        '''
        # Does Article exist? If no - Exception and exit
        # TODO
        # id_or_slag requires checking thesame way like in "Articles" class
        # It's reasonable to make it as a function, which will be used in different classes:
        # e.g. in ArticleTags class
        # /TODO

        try:
            article = Article.get(Article.id == id_or_slug)
            # Does Tag exist? If no - create Tag or error with exit
            try:
                tag, tag_created = Tag.get_or_create(
                    name=req.media['tagname'],
                    defaults={'name': req.media['tagname']}
                )
                # Has Article already marked with the Tag? If no - create record in ArticleTag table
                try:
                    article_tag, article_tag_created = ArticleTag.get_or_create(
                        article_id=id_or_slug,
                        tag_id=tag.id,
                        defaults={'article_id': id_or_slug, 'tag_id': tag.id}
                    )
                    if article_tag_created:
                        resp.body = ujson.dumps({"message": "Article [{}] has marked with Tag [{}] successfully!".format(article.title, tag.name)})
                    else:
                        resp.body = ujson.dumps({"message": "Article [{}] has already marked with Tag [{}]".format(article.title, tag.name)})
                except pw.IntegrityError as ex:
                    resp.body = ujson.dumps({"error": str(ex)})
            except pw.IntegrityError as ex:
                resp.body = ujson.dumps({"error": str(ex)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Article id[{}] does not exist.".format(id_or_slug)})

        resp.status = falcon.HTTP_200


class ArticleTagsUnmark:
    def on_delete(self, req, resp, id, tag_id):
        ''' URI example: /articles/12/tags/4/unmark '''
        # Does Article exist? If no - Exception and exit
        try:
            article = Article.get(Article.id == id)
            # Does the Tag exist? If no - exception and exit.
            try:
                tag = Tag.get(Tag.id == tag_id)
                # Are Article marked with the Tag? If no - exception and exit.
                try:
                    article_tag = ArticleTag.get(ArticleTag.article_id == article.id, ArticleTag.tag_id == tag.id)
                    # Unmark menu or exceptio
                    try:
                        n = article_tag.delete_instance()
                        if n > 0:
                            resp.body = ujson.dumps({"message": "Article [{}] has been unmarked with tag [{}].".format(article.title, tag.name)})
                        else:
                            resp.body = ujson.dumps({"error": "Article [{}] has NOT been unmarked with tag [{}].".format(article.title, tag.name)})
                    except pw.IntegrityError as ex:
                        resp.body = ujson.dumps({"error": str(ex)})
                except pw.DoesNotExist:
                    resp.body = ujson.dumps({"message": "Article [{}] are not marked with tag {}.".format(article.title, tag.name)})
            except pw.DoesNotExist:
                resp.body = ujson.dumps({"message": "Tag id[{}] does not exist.".format(tag_id)})
        except pw.DoesNotExist:
            resp.body = ujson.dumps({"message": "Menu id[{}] does not exist.".format(id)})

        resp.status = falcon.HTTP_200
