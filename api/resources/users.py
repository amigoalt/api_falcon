import uuid
import bcrypt
import ujson
import falcon
import peewee as pw
from playhouse.shortcuts import model_to_dict, dict_to_model, update_model_from_dict
from api.models.db import User


class Users:
    def on_post(self, req, resp):
        if '/login' == req.path:
            credit = req.media
            username = credit['username']
            password = credit['password']
            response = User.login(username, password)
            resp.body = ujson.dumps(response)
            resp.status = falcon.HTTP_200
        elif '/logout' == req.path:
            token = req.auth.split(' ')
            if token[0] == 'Token':
                response = User.logout(token[1])
            else:
                response = {'error': 'Token has to be provided to make Logout posible.'}
            resp.body = ujson.dumps(response)
            resp.status = falcon.HTTP_200
