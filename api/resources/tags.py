import ujson
import falcon
import peewee as pw
from playhouse.shortcuts import model_to_dict, dict_to_model, update_model_from_dict
from api.models.db import Tag


class Tags:
    def on_get(self, req, resp, id=0):
        try:
            tags = Tag.select()
            out = {"tags": [model_to_dict(tag) for tag in tags]}
            resp.body = ujson.dumps(out)
        except Exception as ex:
            resp.body = ujson.dumps({"error": ex})

    def on_post(self, req, resp, id=0):
        ''' POST body example:
            {
                "tag": {
                    "name": "blog"
                }
            }
        '''
        if (int(id) > 0):
            resp.body = ujson.dumps({"error": "POST method is not allowed. To update tag use PATCH."})
            resp.status = falcon.HTTP_200
        elif (req.media['tag']):
            try:
                tag_item = req.media['tag']
                tag_id = Tag.insert([tag_item]).execute()
                resp.body = ujson.dumps({
                    "mesage": "Tag '{}' has been added with id: {}."
                    .format(tag_item['name'], tag_id)})
            except Exception as ex:
                resp.body = ujson.dumps({"error": str(ex)})
        else:
            resp.body = ujson.dumps({"error": "No 'tag' node exist in the POST body."})
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, id):
        try:
            if Tag.delete().where(Tag.id == id).execute():
                resp.body = ujson.dumps({"message": "Tag [{}] has been deleted succsessfully.".format(id)})
            else:
                resp.body = ujson.dumps({"error": "Tag [{}] doesn't exist.".format(id)})
        except Exception as px:
            resp.body = ujson.dumps({'error': str(px)})
        resp.status = falcon.HTTP_200

    def on_patch(self, req, resp, id):
        if 'tag' in req.media.keys():
            try:
                tag_item = Tag.get(Tag.id == id)
                update_model_from_dict(tag_item, req.media["tag"])
                tag_item.save()
                resp.body = ujson.dumps({"message": "Tag [{}] has been updated.".format(tag_item.id)})
            except pw.DoesNotExist as px:
                resp.body = ujson.dumps({'error': str(px)})
            resp.status = falcon.HTTP_200
        else:
            resp.body = ujson.dumps({"error": "'tag' object has not been found in POST body"})
            resp.status = falcon.HTTP_200
