import falcon
import bjoern
import ujson
from api.routes import app
from api.models.wsgilog import WsgiLog


WEB_HOST = '0.0.0.0'
PORT = 9005

# LoggerMiddlware = WsgiLog(app, tofile='wsgi.log')


def main():
    print('Listening on', WEB_HOST + ':' + str(PORT))
    bjoern.run(app, WEB_HOST, int(PORT))
    # bjoern.run(LoggerMiddlware, WEB_HOST, int(PORT))


if __name__ == '__main__':
    main()
