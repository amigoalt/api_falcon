import ujson
from playhouse.shortcuts import model_to_dict, dict_to_model
from api.models.db import init_tables
from api.models.populate import populate_tables

from api.models.db import User

init_tables()
populate_tables()
