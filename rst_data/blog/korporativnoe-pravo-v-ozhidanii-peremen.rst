Сборник статей «Корпоративное право в ожидании перемен» в свободном доступе! Кратко из аннотации: 
В статьях сборника обсуждается, какой путь пройден в реформировании различных аспектов деятельности непубличных компаний в частности и в корпоративном праве в целом. Книга поможет читателю составить представление как об основном круге актуальных вопросов, так и о путях их решения. Авторы активно используют сравнительно-правовой метод.

.. image:: img/articles/korporativnoe-pravo-v-ozhidanii-peremen.jpg
   :alt: Корпоративное право в ожидании перемен
   :class: mt-4 mb-4

`Скачать книгу в формате PDF <http://advokatsmirnova.ru/docs/kuzneczov_korporativnoe-pravo-m-logosobrab.pdf>`_

.. class:: mt-10 text-xs

.. epigraph::

   Источник:

   https://m-lawbooks.ru/index.php/product/kuzneczov-a-a-korporativnoe-pravo-v-ozhidanii-peremen-sbornik-statej-k-20-letiyu-zakona-ob-ooo-2/



