import re
import peewee as pw
from transliterate import translit
from api.models.database import db
from api.models.db import Article
from playhouse.migrate import SqliteMigrator, migrate
migrator = SqliteMigrator(db)


def make_slug(title):
    slug = translit(title, 'ru', reversed=True)
    slug = re.sub('[^\w]+', '-', slug.lower())
    return slug


def up():
    articles = Article.select()
    for article in articles:
        article.slug = make_slug(article.title)

    with db.atomic():
        num_rows = Article.bulk_update(articles, fields=[Article.slug], batch_size=100)

    print("Updated: {} rows".format(num_rows))


def down():
    migrate(
        migrator.drop_column('article', 'slug'),
    )

    slug = pw.TextField(unique=True, null=True)
    migrate(
        migrator.add_column('article', 'slug', slug)
    )
