import peewee as peewee
from api.models.db import db
# migrator = SqliteMigrator(db)

from api.models.db import Rule, User, Tag, Dialect, Article, Menu, ArticleTag, MenuTag
import bcrypt


def populate_tables():
    admin = Rule(name='admin')
    admin.save()
    Rule.create(name='edit')
    Rule.create(name='view')

    User.create(
        username='amigo',
        email='amigoalt@gmail.com',
        name='Антон Гаврилюк',
        rule=admin.id,
        password=bcrypt.hashpw(b'915009', bcrypt.gensalt())
    )

    Dialect.create(name='markdown')

    about_tag = Tag(name='about')
    about_tag.save()
    about_menu = Menu(title='Об адвокате')
    about_menu.save()
    MenuTag.create(tag_id=about_tag.id, menu_id=about_menu.id)

    practice_tag = Tag(name='practice')
    practice_tag.save()
    practice_menu = Menu(title='Практика')
    practice_menu.save()
    MenuTag.create(tag_id=practice_tag.id, menu_id=practice_menu.id)

    blog_tag = Tag(name='blog')
    blog_tag.save()
    blog_menu = Menu(title='Блог')
    blog_menu.save()
    MenuTag.create(tag_id=blog_tag.id, menu_id=blog_menu.id)

    Menu.create(title='Главная')
    Menu.create(title='Контакты')

    Article.create(title='Первая статья')
    Article.create(title='Вторая статья')
    Article.create(
        title="Подобрав кошелёк, можно угодить в тюрьму.. или как всё благополучно окончилось!",
        annotation="Рыбкина О.А. (ФИО изменено) 12.03.2018 г. в 12:06 находилась в магазине “Евроспар” у прилавка в отделе “Домашняя кухня”, когда увидела у ранее ей знакомой Е.Е.Ю, найденный ей кошелёк, принадлежавший…",
        content="Адвокат Московской городской коллегии адвокатов «Московский адвокат». Член Международного союза (содружества) адвокатов.Родилась в г. Йошкар-Оле Республики Марий Эл. В 1994 году закончила с отличием Йошкар-Олинский строительный техникум (отделение правоведение) и поступила на заочное отделение юридического факультета Марийского Государственного Университета. Общий юридический стаж составляет 19 лет, из них в области адвокатуры — 13 лет. Имеет разностороннюю адвокатскую практику. Специализируется на сложных уголовных и гражданских делах, арбитраже. За многолетнюю работу в статусе адвоката имеет значительный опыт по оказанию правовой помощи юридическим лицам по вопросам оспаривания привлечения их к административной ответственности, о переводе прав и обязанностей покупателя по договорам купли-продажи акций, налоговым спорам."
    )


tables = [Rule, User, Tag, Dialect, Article, Menu, ArticleTag, MenuTag]


def up():
    db.create_tables(tables, safe=True)
    populate_tables()


def down():
    db.drop_tables(tables)
