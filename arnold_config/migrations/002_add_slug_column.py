import peewee as pw
from api.models.database import db
from api.models.db import Article
from playhouse.migrate import SqliteMigrator, migrate
migrator = SqliteMigrator(db)


def up():
    slug = pw.TextField(unique=True, null=True)

    migrate(
        migrator.add_column('article', 'slug', slug)
    )


def down():
    migrate(
        migrator.drop_column('article', 'slug'),
    )
