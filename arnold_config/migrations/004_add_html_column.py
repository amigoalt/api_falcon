import peewee as pw
from api.models.database import db
from api.models.db import Article
from playhouse.migrate import SqliteMigrator, migrate
migrator = SqliteMigrator(db)


def up():
    html = pw.TextField(default='')

    migrate(
        migrator.add_column('article', 'html', html)
    )


def down():
    migrate(
        migrator.drop_column('article', 'html'),
    )
